import React from "react";
import renderer from "react-test-renderer";
import Index from "../pages/index";
import Today from "../pages/day/index";
import Enzyme, { mount } from "enzyme";
import MockDate from "mockdate";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

Enzyme.configure({ adapter: new Adapter() });
MockDate.set(1482363367071);

it("check index html", () => {
  const component = renderer.create(<Index />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it('will fail every time', () => {
  const user = {
    createdAt: new Date(),
    id: Math.floor(Math.random() * 20),
    name: 'LeBron James',
  };

  expect(user).toMatchSnapshot();
});

it("will check the matchers and pass", () => {
  const user = {
    createdAt: new Date(),
    id: Math.floor(Math.random() * 20),
    name: "LeBron James"
  };

  expect(user).toMatchSnapshot({
    createdAt: expect.any(Date),
    id: expect.any(Number)
  });
});

it("check today html", () => {
  const component = renderer.create(<Today />);
  let tree = component.toJSON();
  expect(tree).toMatchInlineSnapshot(`
    <div
      className="makeStyles-root-1"
    >
      Hello Day, 
      <span
        onClick={[Function]}
      >
        2016-12-22 7:36:07
      </span>
    </div>
  `);
});

it("check today html again", () => {
  const component = renderer.create(<Today />);
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
  // manually trigger the callback
  // re-rendering
  tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

it("check today html with enzyme", () => {
  const todayInstance = mount(<Today />);
  // manually trigger the callback
  console.log(todayInstance.html());
  todayInstance.find("div span").simulate("click");
  expect(todayInstance.html()).toMatchSnapshot();
  // re-rendering
});
