import React from "react";
import { Today } from "../../views/Today";
import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles({
  root: {
    '&:hover': {
      color: 'red',
    }
  },
})

export default function DayPage() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      Hello Day, <Today />
    </div>
  );
}
