import { useMemo, useState } from "react";

export const Today = () => {
  const [click, setClick] = useState(false);
  const currentDate = useMemo(() => new Date(), []);
  const today = useMemo(
    () => (click ? currentDate.toDateString() : currentDate.toLocaleString()),
    [click, currentDate]
  );
  return <span onClick={() => setClick((ev) => !ev)}>{today}</span>;
};
